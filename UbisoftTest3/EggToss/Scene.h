#include "Framework/GameObject.h"
#include "Framework/TextureManager.h"
#include "Framework/GameManager.h"
#include "Constants.h"

class Bowl : public GameObject
{
public:

	Bowl(Vector2D position, bool isCollidable = true, float speed = 350.0f)
	{
		position.X += m_offset.X;
		position.Y += m_offset.Y;
		SetPosition(position);
		m_speed = speed;
		SetCollidable(isCollidable);
	}

	virtual void Start()
	{
		m_pTexture = TextureManager::GetInstance()->LoadTexture("Resources/Bowl.png");
		m_size = Rect2D(100, 100);
	}

	virtual void Update(float deltaTime)
	{
		auto Pos = GetPosition();
		Pos.X += m_speed * deltaTime;

		//Went outside the screen, reset pos
		if (Pos.X > Constants::SCREEN_WIDTH)
		{
			m_speed *= -1;
			Pos.X = Constants::SCREEN_WIDTH;
		}
		if (Pos.X < 0.0f)
		{
			Pos.X = 0.0f;
			m_speed *= -1;
		}

		SetPosition(Pos);
	}

private:
	Vector2D m_offset = Vector2D(0.0f, 0.0f);
	float m_speed = 350.0f;
};

class Ball : public GameObject
{
public:

	Ball(GameObject* bowl) : m_bowl(bowl)
	{
		Vector2D bowlPosition = m_bowl->GetPosition();
		SetPosition(bowlPosition);
		SetCollidable(true);
	}

	virtual void Start()
	{
		m_pTexture = TextureManager::GetInstance()->LoadTexture("Resources/Ball.png");
		m_size = Rect2D(50, 50);
	}

	virtual void Update(float deltaTime)
	{
		if (m_isJumping)
		{
			auto Pos = GetPosition();
			Pos.Y += m_speed * deltaTime;
			m_speed += m_gravity * deltaTime;
			SetPosition(Pos);

			//Went outside the screen, reset pos
			if (GetPosition().Y > Constants::SCREEN_HEIGHT)
			{
				m_isJumping = false;
				m_speed = m_maxSpeed;
				auto bowlPos = m_bowl->GetPosition();
				SetCollidable(true);
				SetPosition(bowlPos);
			}

			GameManager::GetInstance()->CheckCollision(this);
		}
		else
		{
			auto bowlPosition = m_bowl->GetPosition();
			SetPosition(bowlPosition);
		}

	}

	virtual void OnKeyInput()
	{
		m_isJumping = true;
	}

	virtual void OnCollision(GameObject* gameobject)
	{
		float ballY = GetPosition().Y;
		float bowlY = gameobject->GetPosition().Y;

		if (ballY > bowlY)
		{
			SetCollidable(false);
			return;
		}

		m_isJumping = false;
		m_bowl = gameobject;
		m_speed = m_maxSpeed;
		gameobject->SetCollidable(false);
		m_bowlCount++;
	}

	int GetBowlCount()
	{
		return m_bowlCount;
	}

private:
	Vector2D m_Offset = Vector2D(0.0f, 0.0f);
	float m_timer = 0.0f;
	float m_gravity = 800.0f;
	float m_speed = -600.0f;
	float m_maxSpeed = -600.0f;

	bool m_isJumping = false;
	int m_bowlCount = 0;

	GameObject* m_bowl;

	void SeToBowlPosition()
	{
		Vector2D bowlPosition = m_bowl->GetPosition();
		SetPosition(bowlPosition);
	}
};


class Ground : public GameObject
{
public:
	virtual void Start()
	{
		m_pTexture = TextureManager::GetInstance()->LoadTexture("Resources/Ground.png");
		m_position = Vector2D(-10.0f, 300.0f);
	}
};


