#pragma once

struct Rect2D
{
    Rect2D(unsigned int width, unsigned int height)
        : m_width(width)
        , m_height(height)
    {
    }

    Rect2D()
        : m_width(0)
        , m_height(0)
    {
    }

    bool    IsInvalid() const
    {
        return m_width == 0 || m_height == 0;
    }

    unsigned int GetWidth() const { return m_width; }
    unsigned int GetHeight() const { return m_height; }

private:

    unsigned int m_width;
    unsigned int m_height;
};
