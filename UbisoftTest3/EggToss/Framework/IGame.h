#pragma once

class IGame
{
public:

    virtual void StartGame() = 0;
    virtual void UpdateGame(float deltaTime) = 0;
};
