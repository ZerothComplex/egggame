#include "GameManager.h"
#include "GameObject.h"
#include "Texture.h"
#include "SDL.h"
#include "SDL_ttf.h"
#include "../Constants.h"

GameManager* GameManager::m_instance = nullptr;

GameManager* GameManager::GetInstance()
{
	if (!m_instance)
	{
		m_instance = new GameManager;
		TTF_Init();
	}

	return m_instance;
}

void GameManager::DeleteInstance()
{
	if (m_instance)
	{
		delete m_instance;
	}
}

GameObject* GameManager::AddObject(GameObject* pObject)
{
	m_pendingPush.push_back(pObject);

	if (pObject->IsCollidable())
		m_collidableGameObjects.push_back(pObject);

	return pObject;
}

void GameManager::RemoveObject(GameObject* pObject)
{
	for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++)
	{
		if (*it == pObject)
		{
			m_pendingRemove.push_back(pObject);
			break;
		}
	}
}

void GameManager::Update(float deltaTime)
{
	// Are there any pending game objects to be processed ?
	for (auto it = m_pendingPush.begin(); it != m_pendingPush.end(); it++)
	{
		auto pObject = *it;
		if (pObject)
		{
			pObject->Start();
			m_gameObjects.push_back(pObject);
		}
	}
	m_pendingPush.clear();

	for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++)
	{
		auto obj = *it;
		if (obj->IsMarkedForDelete())
		{
			m_pendingRemove.push_back(obj);
			continue;
		}

		obj->Update(deltaTime);
	}

	// Are there any pending game objects to be removed ?
	for (auto it = m_pendingRemove.begin(); it != m_pendingRemove.end(); it++)
	{
		auto objectsIt = m_gameObjects.begin();
		while (objectsIt != m_gameObjects.end())
		{
			if (*it == *objectsIt)
			{
				delete* objectsIt;

				objectsIt = m_gameObjects.erase(objectsIt);
				continue;
			}
			objectsIt++;
		}
	}
	m_pendingRemove.clear();
}

void GameManager::Render()
{
	for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++)
	{
		GameObject* pObject = *it;
		Texture* pTexture = pObject->GetTexture();

		if (pTexture && pObject->IsVisible())
		{
			SDL_Rect Dest;
			Dest.x = (int)pObject->GetPosition().X;
			Dest.y = (int)pObject->GetPosition().Y;

			const Rect2D& size = pObject->GetSize();
			if (size.IsInvalid())
			{
				Dest.w = pTexture->GetWidth();
				Dest.h = pTexture->GetHeight();
			}
			else
			{
				Dest.w = size.GetWidth();
				Dest.h = size.GetHeight();
			}

			SDL_RenderCopy(m_pRenderer, pObject->GetTexture()->GetSDLTexture(), NULL, &Dest);
		}
	}
}

void GameManager::CheckCollision(GameObject* pObject)
{
	SDL_Rect source;
	source.x = (int)pObject->GetPosition().X;
	source.y = (int)pObject->GetPosition().Y;

	const Rect2D& size = pObject->GetSize();
	source.w = size.GetWidth();
	source.h = size.GetHeight();

	for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++)
	{
		GameObject* pObject2 = *it;

		if (pObject2 == pObject)
			continue;

		if (pObject2->IsVisible() && pObject2->IsCollidable())
		{
			SDL_Rect dest;
			dest.x = (int)pObject2->GetPosition().X;
			dest.y = (int)pObject2->GetPosition().Y;

			const Rect2D& size = pObject2->GetSize();
			dest.w = size.GetWidth();
			dest.h = size.GetHeight();

			if (SDL_HasIntersection(&source, &dest))
			{
				pObject->OnCollision(pObject2);
			}
		}
	}
}

void GameManager::OnKeyInput()
{
	// Send key input event to all gameObjects
	for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++)
	{
		auto pObject = *it;
		if (pObject)
		{
			pObject->OnKeyInput();
		}
	}
}

void GameManager::UpdateUI(const char* text)
{
	int fontsize = 25;
	int t_width = 50; // width of the loaded font-texture
	int t_height = 50; // height of the loaded font-texture
	SDL_Color text_color = { 255,255,255,255 };
	const char* fontpath = "Resources/OpenSans-Regular.ttf";
	TTF_Font* font = TTF_OpenFont(fontpath, fontsize);
	SDL_Texture* ftexture = NULL; // our font-texture

	//// check to see that the font was loaded correctly
	if (font != NULL) 
	{
		// now create a surface from the font
		SDL_Surface* text_surface = TTF_RenderText_Solid(font, text, text_color);

		// render the text surface
		if (text_surface != NULL) 
		{
			// create a texture from the surface
			ftexture = SDL_CreateTextureFromSurface(m_pRenderer, text_surface);

			if (ftexture != NULL) 
			{
				t_width = text_surface->w; // assign the width of the texture
				t_height = text_surface->h; // assign the height of the texture

				// clean up after ourselves (destroy the surface)
				SDL_FreeSurface(text_surface);

				int x = Constants::SCREEN_WIDTH / 2;
				int y = 0;
				SDL_Rect dst = { x, 0, t_width, t_height };
				SDL_RenderCopy(m_pRenderer, ftexture, NULL, &dst);
				SDL_RenderPresent(m_pRenderer);
			}
		}
	}
}