#pragma once

struct Vector2D
{
public:

    Vector2D()
        : X(0)
        , Y(0)
    {
    }
    
    Vector2D(float x, float y)
        : X(x)
        , Y(y)
    {
    }

    float X;
    float Y;
};
