#include "GameEngine.h"
#include "IGame.h"
#include "GameManager.h"

#include "SDL.h"
#include "SDL_image.h"

#include <assert.h>
#include <iostream>
#include <ctime>

GameEngine::GameEngine(IGame* pGame)
	: m_pGame(pGame)
{
}

int GameEngine::StartEngine(int width, int height)
{
	assert(m_pGame != nullptr);

	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window* win = SDL_CreateWindow("Ubisoft Test Game", 100, 100, width, height, SDL_WINDOW_SHOWN);
	if (win == nullptr)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		IMG_Quit();
		SDL_Quit();
		return 1;
	}

	SDL_Renderer* ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr)
	{
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		IMG_Quit();
		SDL_Quit();
		return 1;
	}

	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);

	GameManager::GetInstance()->SetRenderer(ren);

	m_pGame->StartGame();

	std::clock_t lastFrameTime = std::clock();

	SDL_Event event;
	bool    keepRunning = true;
	bool	keyWasPressed = false;

	while (keepRunning)
	{
		// Calculate Delta
		std::clock_t currentFrameTime = std::clock();
		float delta = (float)(currentFrameTime - lastFrameTime) / CLOCKS_PER_SEC;
		lastFrameTime = currentFrameTime;

		m_pGame->UpdateGame(delta);
		GameManager::GetInstance()->Update(delta);

		SDL_RenderClear(ren);

		GameManager::GetInstance()->Render();

		SDL_RenderPresent(ren);

		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				//If a key was pressed
			case SDL_KEYDOWN:	GameManager::GetInstance()->OnKeyInput();	break;
			case SDL_QUIT:  keepRunning = false;   break;
			}
		}
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);

	IMG_Quit();
	SDL_Quit();

	return 0;
}
