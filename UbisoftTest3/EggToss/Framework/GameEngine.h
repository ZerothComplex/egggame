#pragma once

class IGame;

class GameEngine
{
public:
    GameEngine(IGame* pGame);

    int             StartEngine(int width, int height);
private:
    IGame*          m_pGame;
};
