#pragma once
#include "Vector2D.h"
#include "Rect2D.h"

class Texture;

class GameObject
{
public:
    GameObject();
    virtual ~GameObject();

    virtual void    Start();
    virtual void    Update(float deltaTime);
	virtual void	OnKeyInput();
	virtual void	OnCollision(GameObject* collider);


    void            SetVisible(bool visible) { m_visible = visible; }
    bool            IsVisible() const { return m_visible; }

    bool            IsMarkedForDelete() const { return m_markedForDelete; }
    void            Destroy() { m_markedForDelete = true; }

    Texture*        GetTexture() const { return m_pTexture; }

    void            SetPosition(Vector2D& position) { m_position = position; }
	const Vector2D& GetPosition() const { return m_position; }

    void            SetSize(Rect2D& size) { m_size = size; }
    const Rect2D&   GetSize() const { return m_size; }

	void			SetCollidable(bool collidable) { m_collidable = collidable;}
	bool	 		IsCollidable() const { return m_collidable; }

protected:

    Texture*        m_pTexture;
    Vector2D        m_position;
    Rect2D          m_size;

    bool            m_visible;
    bool            m_markedForDelete;
	bool			m_collidable;
};
