#include "Texture.h"
#include "SDL.h"

Texture::Texture(SDL_Texture* pTexture, int width, int height)
    : m_pTexture(pTexture)
    , m_width(width)
    , m_height(height)
{
}

Texture::~Texture()
{
    SDL_DestroyTexture(m_pTexture);
}