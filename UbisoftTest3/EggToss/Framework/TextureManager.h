#pragma once
#include <string>
#include <unordered_map>

class Texture;

class TextureManager
{
public:
    static TextureManager*                      GetInstance();

    Texture*                                    LoadTexture(std::string fileName);

private:
    std::unordered_map<std::string, Texture*>   m_textures;

    static TextureManager*                      m_instance;
};
