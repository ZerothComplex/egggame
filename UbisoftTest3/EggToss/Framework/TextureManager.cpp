#include "TextureManager.h"
#include "Texture.h"
#include "GameManager.h"

#include "SDL.h"
#include "SDL_image.h"

TextureManager* TextureManager::m_instance = nullptr;

TextureManager* TextureManager::GetInstance()
{
    if (!m_instance)
    {
        m_instance = new TextureManager;
    }

    return m_instance;
}

Texture*    TextureManager::LoadTexture(std::string fileName)
{
    Texture* pTexture = m_textures[fileName];
    if (!pTexture)
    {
        SDL_Texture *pTex = IMG_LoadTexture(GameManager::GetInstance()->GetRenderer(), fileName.c_str());
        if (pTex == nullptr)
        {
            return nullptr;
        }

        int w, h;
        SDL_QueryTexture(pTex, NULL, NULL, &w, &h);

        pTexture = new Texture(pTex, w, h);
        m_textures[fileName] = pTexture;
    }

    return pTexture;
}