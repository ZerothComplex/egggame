#pragma once

struct SDL_Texture;

class Texture
{
public:
    Texture(SDL_Texture* pTexture, int width, int height);
    ~Texture();

    SDL_Texture*        GetSDLTexture() const { return m_pTexture; }

    int                 GetWidth() const { return m_width; }
    int                 GetHeight() const { return m_height; }
    
private:
    SDL_Texture*        m_pTexture;

    int                 m_width;
    int                 m_height;
};
