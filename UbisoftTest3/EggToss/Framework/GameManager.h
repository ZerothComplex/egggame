#pragma once
#include <vector>

class GameObject;

struct SDL_Renderer;

class GameManager
{
public:
    static GameManager*         GetInstance();
    static void                 DeleteInstance();

    void                        SetRenderer(SDL_Renderer* renderer) { m_pRenderer = renderer; }
    SDL_Renderer*               GetRenderer() const { return m_pRenderer; }

    GameObject*                 AddObject(GameObject* pObject);
    void                        RemoveObject(GameObject* pObject);

    void                        Update(float deltaTime);
    void                        Render();
	void						OnKeyInput();
	void						CheckCollision(GameObject* pObject);
	void						UpdateUI(const char* text);

private:

    GameManager()
    {
    }

    std::vector<GameObject*>    m_gameObjects;
    std::vector<GameObject*>    m_pendingPush;
    std::vector<GameObject*>    m_pendingRemove;
	std::vector<GameObject*>	m_collidableGameObjects;

    SDL_Renderer*               m_pRenderer;

    static GameManager*         m_instance;
};
