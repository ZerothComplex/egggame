#include <iostream>
#include <windows.h>

#include "Framework/GameEngine.h"
#include "Framework/IGame.h"
#include "Framework/GameObject.h"
#include "Framework/TextureManager.h"
#include "Framework/GameManager.h"
#include "Scene.h"

// Game Logic Goes in the Game Class.
class BallGame : public IGame
{
private:
	Ball*						m_pBall;

	std::vector <GameObject>	m_gameObejcts;

	float						m_time;

	Vector2D bowlPos[3] = {
							Vector2D(Constants::SCREEN_WIDTH / 2, Constants::SCREEN_HEIGHT * 3 / 4),
							Vector2D(Constants::SCREEN_WIDTH / 2, Constants::SCREEN_HEIGHT / 2),
							Vector2D(Constants::SCREEN_WIDTH / 2, Constants::SCREEN_HEIGHT / 4)
	};
	

public:

	virtual void StartGame() override
	{
		GameManager::GetInstance()->AddObject(new Ground);

		Bowl* bowls[3] = {
							new Bowl(bowlPos[0], false),
							new Bowl(bowlPos[1], true, 100),
							new Bowl(bowlPos[2], true, 200)
		};

		m_pBall = new Ball(bowls[0]);

		for (int i = 0; i < 3; i++)
		{
			GameManager::GetInstance()->AddObject(bowls[i]);
		}

		m_pBall = (Ball*)GameManager::GetInstance()->AddObject(m_pBall);
	}

	virtual void UpdateGame(float deltaTime) override
	{
		int score = m_pBall->GetBowlCount()* 10;
		m_time += deltaTime;
		std::string status = "Score: " + std::to_string(score) + "     Time: " + std::to_string((int)m_time);
		GameManager::GetInstance()->UpdateUI(status.c_str());
	}
};


int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
	BallGame game;
	GameEngine engine(&game);
	engine.StartEngine(Constants::SCREEN_WIDTH, Constants::SCREEN_HEIGHT);

	return true;
}